package com.timbuchalka;

public class SalariedEmployee extends StaffMember {
    private double salary;
    private double bonus;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "\n" +
                "\tid : " + id + "\n" +
                "\tname : " + name + "\n" +
                "\taddress : " + address + "\n" +
                "\tsalary : " + salary + "\n" +
                "\tbonus : " + bonus + "\n" +
                "\t------------------------------------\n";
    }

    @Override
    public double pay() {
        return 0;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public double getBonus() {
        return bonus;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }
}
