package com.timbuchalka;

import java.awt.print.Book;
import java.util.*;

public class StartProgram {

    ArrayList<StaffMember> staffMembers = new ArrayList<>();

    void start(){
        do {
            menu();
        }while (true);
    }

    private void menu() {
        String inputOption;
        Scanner sc = new Scanner(System.in);
        System.out.println("\n\t\t\t\t\t\t------------------------------------");
        System.out.println("\t\t\t\t\t\t|            StaffMember           |");
        System.out.println("\t\t\t\t\t\t------------------------------------");
        System.out.println("\t\t 1). Add Employee \t 2). Display \t 3). Edit \t4). Remove \t 5). Exit");
        System.out.println("\t\t----------------------------------------------------------------------");
        System.out.print("\n=> Choose option(1-4): ");
        inputOption = sc.nextLine();
        if (isStringOnlyNumberic(inputOption)) {
            switch (Integer.parseInt(inputOption)) {
                case 1:
                    addEmployee();
                    break;
                case 2:
                    display();
                    break;
                case 3:
                    updateData();
                    break;
                case 4:
                    deleteData();
                    break;
                case 5:
                    exitProgram();
                    break;
            }
        }
    }

    // add function

    private void addEmployee() {
        String inputOption;
        Scanner sc = new Scanner(System.in);
        System.out.println("\n\t\t\t\t\t============ AddStaffMember ============");
        System.out.println("\t 1). Volunteer \t 2). Hourly Emp \t 3). Salaried Emp \t 4). Back");
        System.out.println("\t------------------------------------------------------------------");
        System.out.print("=> Choose option(1-4): ");

        inputOption = sc.nextLine();
        if (isStringOnlyNumberic(inputOption)) {
            switch (Integer.parseInt(inputOption)) {
                case 1:
                    addVolunteer();
                    addEmployee();
                    break;
                case 2:
                    addHourlyEmp();
                    addEmployee();
                    break;
                case 3:
                    addSalariedEmp();
                    addEmployee();
                    break;
                case 4:
                    menu();
                    break;
            }
        }
    }

    private void addVolunteer(){
        String name, id, address;
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("\n\t\t============ INSERT INTO ============");
            System.out.print("\n=> Enter Staff Member's ID        : ");
            id = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Name      :  ");
            name = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Address : ");
            address = sc.nextLine();
            if (isStringOnlyNumberic(id) && isStringOnlyAlphabet(name) && isStringOnlyAlphabet(address)) {
                Volunteer volunteer = new Volunteer(Integer.parseInt(id), name, address);
                this.staffMembers.add(volunteer);
            }
            else {
                System.out.println("Invalid Data");
                addEmployee();
            }
        }
        catch (Exception e) {
            System.out.println("Invalid Data");
            addEmployee();
        }
    }

    private void addHourlyEmp(){
        Scanner sc = new Scanner(System.in);
        String name, id, address, hourworked, rate;

        try {
            System.out.println("\n\t\t============ INSERT INTO ============");
            System.out.print("\n=> Enter Staff Member's ID         : ");
            id = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Name       :  ");
            name = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Address    : ");
            address = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's HourWorded :  ");
            hourworked = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Rate       : ");
            rate = sc.nextLine();

            if (isStringOnlyNumberic(id) && isStringOnlyAlphabet(name) && isStringOnlyAlphabet(address) && isStringOnlyNumbericDouble(hourworked) && isStringOnlyNumbericDouble(rate)){
                HourlyEmployee employee = new HourlyEmployee(Integer.parseInt(id), name, address, Integer.parseInt(hourworked), Double.parseDouble(rate));
                this.staffMembers.add(employee);
            }
            else {
                System.out.println("Invalid Data");
                addEmployee();
            }
        }
        catch (Exception e){
            System.out.println("Invalid Data");
            addEmployee();
        }
    }

    private void addSalariedEmp(){
        Scanner sc = new Scanner(System.in);
        String name, id, address, salary, bonus;
        try {
            System.out.println("\n\t\t============ INSERT INTO ============");
            System.out.print("\n=> Enter Staff Member's ID       : ");
            id = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Name     :  ");
            name = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Address  : ");
            address = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Salary   :  ");
            salary = sc.nextLine();
            System.out.print("\n=> Enter Staff Member's Bonus    : ");
            bonus = sc.nextLine();

            if (isStringOnlyNumberic(id) && isStringOnlyAlphabet(name) && isStringOnlyAlphabet(address) && isStringOnlyNumbericDouble(salary) && isStringOnlyNumbericDouble(bonus)) {
                SalariedEmployee employee = new SalariedEmployee(Integer.parseInt(id), name, address, Double.parseDouble(salary), Double.parseDouble(bonus));
                staffMembers.add(employee);
            }
            else {
                System.out.println("Invalid Data");
                addEmployee();
            }
        }
        catch (Exception e) {
            System.out.println("Invalid Data");
            addEmployee();
        }
    }

    private void display(){
        Scanner sc  = new Scanner(System.in);
        String input;
        System.out.println("\t1). Sort by ID, 2).Sort By Name");
        input = sc.nextLine();
        if (isStringOnlyNumberic(input)){
            switch (Integer.parseInt(input)) {
                case 1:
                    sort("id", true);
                    break;
                case 2:
                    sort("name", true);
                    break;
            }
        }
        System.out.println("\n\t\t================== Display Data ==================");
        for (StaffMember s:
             staffMembers) {
            System.out.println(s.toString());
        }
        System.out.println("===================================================");
    }

    private void sort(String sortBy, boolean asc){
        if(sortBy.equals("id")){
            staffMembers.sort((a, b) -> (asc ? a.getId() - b.getId() : b.getId() - a.getId()));
        }else if(sortBy.equals("name")){
            this.staffMembers.sort((a, b) -> (asc ? a.getName().compareTo(b.getName()) : b.getName().compareTo(a.getName())));
        }
    }

    // Update Function
    private void updateData(){
        Scanner input = new Scanner(System.in);
        String id;
        System.out.println("\n\t\t============ EDIT INTO ============");
        System.out.print("\n=> Enter Employee ID to Update: ");
        id = input.nextLine();
        if (isStringOnlyNumberic(id)) {
            StaffMember s = null;
            for (StaffMember check : this.staffMembers){
                if (check.getId() == Integer.parseInt(id)){
                    s = check;
                    System.out.println(s.toString());
                    break;
                }
            }
            if (s == null)
            {
                return;
            }

            if (s instanceof Volunteer) {
                // cast object reference to child
                Volunteer v = (Volunteer) s;
                String name, address;
                System.out.println("\t\t====== New INFORMATION OF STAFF MEMBER ======");
                System.out.print("\n=> Enter Staff Member's Name : ");
                name = input.nextLine();
                System.out.print("\n=> Enter Staff Member's Address : ");
                address = input.nextLine();

                v.name = name;
                v.address = address;
            } else if (s instanceof HourlyEmployee) {
                // cast object reference to child
                HourlyEmployee e = (HourlyEmployee) s;
                String name, address, houlywork;
                System.out.println("\t\t====== New INFORMATION OF STAFF MEMBER ======");
                System.out.print("\n=> Enter Staff Member's Name : ");
                name = input.nextLine();
                System.out.print("\n=> Enter Staff Member's Address : ");
                address = input.nextLine();
                System.out.print("\n=> Enter Staff Member's HourlyWorked : ");
                houlywork = input.nextLine();

                e.name = name;
                e.address = address;
                e.setHourlyWorked( Integer.parseInt(houlywork));
            }
            else if (s instanceof SalariedEmployee) {
                SalariedEmployee sa =  (SalariedEmployee)s;
                String name, address, salary, bouns;
                System.out.println("\t\t====== New INFORMATION OF STAFF MEMBER ======");
                System.out.print("\n=> Enter Staff Member's Name : ");
                name = input.nextLine();
                System.out.print("\n=> Enter Staff Member's Address : ");
                address = input.nextLine();
                System.out.print("\n=> Enter Staff Member's Salary : ");
                salary = input.nextLine();
                System.out.print("\n=> Enter Staff Member's bonus : ");
                bouns = input.nextLine();

                sa.name = name;
                sa.address = address;
                sa.setSalary(Double.parseDouble(salary));
                sa.setBonus(Double.parseDouble(bouns));
            }
        }
    }

    // Remove Function
    private void deleteData(){
        Scanner input = new Scanner(System.in);
        String index;
        System.out.println("\n\t\t============ EDIT INTO ============");
        System.out.print("\n=> Enter Employee ID to Remove: ");
        index = input.nextLine();
        if (isStringOnlyNumberic(index)) {
            StaffMember s = null;
                for (StaffMember check : this.staffMembers){
                    if (check.getId() == Integer.parseInt(index)){
                        s = check;
                        System.out.println(s.toString());
                        break;
                    }
                }
                if (s == null)
                {
                    return;
                }
            staffMembers.remove(index);
        }
        else {
            System.out.println("Invalid Data Please try Again!!!");
            addEmployee();
        }
    }

    // Exit Function
    private void exitProgram() {
        Scanner sc = new Scanner(System.in);
        String decision;
        boolean yn = true;
        try {
            System.out.print("\t\n=> Do you went to Exit Program | yes Or no | : ");
            decision = sc.nextLine();
            if (isStringOnlyAlphabetexit(decision)) {
                switch (decision) {
                    case "yes":
                        System.out.printf("\n\t\t\t\t--------------------------%n");
                        System.out.printf("\t\t\t\t| (^_^)  Good-Bye  (^_^) |%n");
                        System.out.printf("\t\t\t\t--------------------------%n");
                        System.exit(0);
                        break;
                    case "no":
                        menu();
                        break;
                }
            } else {
                System.out.println("\t=> Data Incorrect, Please Try Again!!!");
                menu();
            }
        } catch (Exception ex) {
            menu();
        }
    }

    public static boolean isStringOnlyAlphabet(String str)
    {
        return ((!str.equals(""))
                && (str != null)
                && (str.matches("^[ A-Za-z]+$")));
    }

    public static boolean isStringOnlyAlphabetexit(String str)
    {
        return ((!str.equals(""))
                && (str != null)
                && (str.matches("^[a-zA-Z]*$")));
    }

    public static boolean isStringOnlyNumberic(String str)
    {
        return ((str != null)
                && (!str.equals(""))
                && (str.matches("^\\d+$")));
    }

    public static boolean isStringOnlyNumbericDouble(String str)
    {
        return ((!str.equals(""))
                && (str != null)
                && (str.matches("-?\\d+(\\.\\d+)?")));
    }
}
