package com.timbuchalka;

public class HourlyEmployee extends StaffMember{
    private int hourlyWorked;
    private double rate;
    public HourlyEmployee(int id, String name, String address, int hourlyWorked, double rate){
        super(id, name, address);
        this.hourlyWorked = hourlyWorked;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "\n" +
                "\tid : " + id +
                "\tname : " + name + "\n" +
                "\taddress : " + address + "\n" +
                "\thourlyWorked : " + hourlyWorked + "\n" +
                "\trate : " + rate + "\n" +
                "\t------------------------------------\n";
    }

    @Override
    public double pay() {
        return 0;
    }

    public void setHourlyWorked(int hourlyWorked) {
        this.hourlyWorked = hourlyWorked;
    }

    public int getHourlyWorked() {
        return hourlyWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
